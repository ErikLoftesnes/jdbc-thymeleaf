package com.example.jdbcthymeleaf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JdbcThymeleafApplication {

    public static void main(String[] args) {
        SpringApplication.run(JdbcThymeleafApplication.class, args);
    }

}
